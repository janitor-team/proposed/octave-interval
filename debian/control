Source: octave-interval
Section: math
Priority: optional
Maintainer: Debian Octave Group <team+pkg-octave-team@tracker.debian.org>
Uploaders: Oliver Heimlich <oheim@posteo.de>,
Build-Depends-Indep: texinfo, imagemagick, gnuplot-nox
Build-Depends: debhelper-compat (= 13),
               dh-octave (>= 1.2.3),
               dh-sequence-octave,
               ghostscript,
               libmpfr-dev (>= 3.1.0)
Standards-Version: 4.6.1
Homepage: https://octave.sourceforge.io/interval/
Vcs-Git: https://salsa.debian.org/pkg-octave-team/octave-interval.git
Vcs-Browser: https://salsa.debian.org/pkg-octave-team/octave-interval
Testsuite: autopkgtest-pkg-octave
Rules-Requires-Root: no

Package: octave-interval
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${octave:Depends}, ${shlibs:Depends},
Suggests: octave-interval-doc
Description: real-valued interval arithmetic for Octave
 ${octave:Upstream-Description}
 .
 This Octave add-on package is part of the Octave-Forge project.

Section: doc
Package: octave-interval-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: w3m | www-browser
Enhances: octave-interval
Description: real-valued interval arithmetic for Octave (arch-indep files)
 ${octave:Upstream-Description}
 .
 This Octave add-on package is part of the Octave-Forge project.
 .
 This package provides documentation in HTML format for the octave-interval
 package.
