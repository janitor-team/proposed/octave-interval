#!/usr/bin/make -f
# -*- makefile -*-

# Copyright (c) 2015, 2016  Oliver Heimlich <oheim@posteo.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

%:
	dh $@ --buildsystem=octave

pkg = $(shell awk '/^Name:/ {print $$2; exit 0}' DESCRIPTION)
pkg_version = $(shell awk '/^Version:/ {print $$2; exit 0}' DESCRIPTION)
package = $(shell awk '/^Package:/ {print $$2; exit 0}' debian/control)
debpkg = debian/$(package)

# For reproducibility and portability,
# enable SSE2 only on architectures where it is supported by all processors.
ifeq ($(DEB_HOST_ARCH_CPU),amd64)
export CONF_FLAG_SSE2 = --enable-sse2
else
export CONF_FLAG_SSE2 = --disable-sse2
endif

# Don't install NEWS.gz, dh-octave will handle this
DEB_INSTALL_DOCS_ALL =
doc_dir = $(CURDIR)/$(debpkg)-doc/usr/share/doc/$(package)

override_dh_auto_configure:
	(cd src/crlibm ; autoreconf -i)
	dh_auto_configure

ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
override_dh_auto_install-indep:
	dh_auto_install
	# The documentation build needs to load the package,
	# so we have to set paths for the build process.
	OCTAVE='octave --no-window-system --no-init-file'	\
		make -C doc manual.html
	# dh_installdocs does not support subdirectories,
	# so we move everything in place
	mkdir -p $(doc_dir)/image
	cp doc/manual.html $(doc_dir)/
	cp doc/image/*.png doc/image/*.svg $(doc_dir)/image/
	rm -rf $(CURDIR)/$(debpkg)/$(mpath)/*/doc
endif

override_dh_auto_clean:
	dh_auto_clean
	[ ! -f Makefile ] || $(MAKE) clean
	[ ! -f doc/Makefile ] || $(MAKE) -C doc clean
	[ ! -f src/crlibm/Makefile ] || $(MAKE) -C src/crlibm maintainer-clean
