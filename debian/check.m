## The interval package contains additional .tst files,
## which are not checked by default.
disp ("Checking supplementary TST files ...");
npass_total = 0;
ntest_total = 0;
nxfail_total = 0;
nskip_total = 0;
for file = {dir("./inst/test/*tst").name}
  [npass, ntest, nxfail, nskip] = test (file {1}, "quiet", stdout);
  printf ("%d test%s, %d passed, %d known failure%s, %d skipped\n",
          ntest, ifelse (ntest > 1, "s", ""), npass, nxfail,
          ifelse (nxfail > 1, "s", ""), nskip);
  npass_total += npass;
  ntest_total += ntest;
  nxfail_total += nxfail;
  nskip_total += nskip;
endfor
# Signal an error if there is any failure
if ntest_total != npass_total + nxfail_total + nskip_total
  error ("Failure in TST files")
endif
